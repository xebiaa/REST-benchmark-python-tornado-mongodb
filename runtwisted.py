from twisted.web import server, resource
from twisted.internet import reactor
from json import dumps, loads
import sys

import pymongo
DB = pymongo.Connection().perftest
find_one = DB.messages.find_one
save = DB.messages.save


class Root(resource.Resource):
    isLeaf = True

    def render_GET(self, request):
        global dumps, find_one
        try:
            id = int(request.uri.rsplit('/', 1)[-1])
            msg = find_one({"_id": id})
            msg['id'] = msg['_id']
            del msg['_id']
            return dumps(msg)
        except:
            request.setResponseCode(404)
            return "Invalid request"

    def render_POST(self, request):
        global loads, save
        try:
            id = request.uri.rsplit('/', 1)[-1]
            msg = loads(request.content.getvalue())
            if id != msg['id']:
                request.setResponseCode(409)
                return ""
            msg["_id"] = int(id)
            del msg['id']
            save(msg)
        except:
            print 'error?', sys.exc_info()
        return ""



site = server.Site(Root())
reactor.listenTCP(8080, site)

print '*' * 80
print '  Server started'
print '*' * 80

reactor.run()


# vim:sw=4:et:ai
