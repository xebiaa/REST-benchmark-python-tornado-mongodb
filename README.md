REST performance test Twisted/Tornado + MongoDB
===============================================

Very fast implementation of an async server.

The Tornado version is by far the fastest.

To get it started:

1. Install Tornado:

  $ easy_install tornado

2. Run the server:

  $ python runtornado.py

3. The server is started on port 8888.


