
from tornado import httpserver, ioloop
from json import dumps, loads
import sys

import pymongo
DB = pymongo.Connection().perftest
find_one = DB.messages.find_one
save = DB.messages.save



def handle_request(request):
    method = request.method
    if method == "GET":
        global dumps, find_one
        try:
            id = int(request.uri.rsplit('/', 1)[-1])
            msg = find_one({"_id": id})
            msg['id'] = msg['_id']
            del msg['_id']
            message = dumps(msg)
	    request.write("HTTP/1.1 200 OK\r\nContent-Length: %d\r\n\r\n%s" % (
                 len(message), message))
        except:
            request.write("HTTP/1.1 404 NOT FOUND\r\nContent-Length: 0\r\n\r\n")
    elif method == "POST": 
        global loads, save
        try:
            id = request.uri.rsplit('/', 1)[-1]
            msg = loads(request.body)
            if id != msg['id']:
                request.write("HTTP/1.1 409 CONFLICT\r\nContent-Length: 0\r\n\r\n")
                request.finish()
                return
            msg["_id"] = int(id)
            del msg['id']
            save(msg)
            request.write("HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n")
        except:
            print 'error?', sys.exc_info()
            request.write("HTTP/1.1 500 INTERNAL SERVER ERROR\r\nContent-Length: 0\r\n\r\n")
    request.finish()

http_server = httpserver.HTTPServer(handle_request)
http_server.bind(8888)
http_server.start(0) # Forks multiple sub-processes
ioloop.IOLoop.instance().start()

# vim:sw=4:et:ai
